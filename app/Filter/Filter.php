<?php

namespace App\Filter;

use Illuminate\Http\Request;

abstract class Filter
{
    protected $request;
    protected $builder;
    protected $filters = [];

    /**
     * ThreadsFilter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {

        $this->request = $request;
    }

    /**
     * Executes filtering for allowed methods.
     *
     * @param $builder
     * @return mixed
     */
    public function apply($builder)
    {
        $this->builder = $builder;



        foreach ($this->getFilter() as $filter => $value) {

            if (method_exists($this, $filter)) {

                $this->$filter($value);
            }
        }

        return $this->builder;
    }


    /**
     * Filters input into acceptable filter array.
     *
     * @return array
     */
    protected function getFilter()
    {
        return $this->request->only($this->filters);
    }
}