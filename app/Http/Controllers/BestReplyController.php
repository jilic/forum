<?php

namespace App\Http\Controllers;

use App\Reply;

class BestReplyController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function store(Reply $reply)
    {
        //abort_if($reply->thread->creator->id != $reply->owner->id, 401);
        $this->authorize('update', $reply->thread);

        $reply->thread->markBestReply($reply);
    }
}
