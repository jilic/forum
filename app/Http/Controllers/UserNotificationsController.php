<?php

namespace App\Http\Controllers;

use App\User;

class UserNotificationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Fetch all unread messages.
     *
     * @return mixed
     */
    public function index()
    {
        return auth()->user()->unreadNotifications;
    }

    /**
     * Mark notification as read.
     *
     * @param User $user
     * @param $notificationId
     */
    public function destroy(User $user, $notificationId)
    {
        auth()->user()->notifications()->findOrFail($notificationId)->markAsRead();
    }
}
