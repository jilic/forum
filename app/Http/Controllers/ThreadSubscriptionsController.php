<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;

class ThreadSubscriptionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create a new subscription for the thread.
     *
     * @param $channel
     * @param Thread $thread
     */
    public function store($channel, Thread $thread)
    {
        $thread->subscribe();
    }

    /**
     * Delete user subscription to thread.
     *
     * @param $channel
     * @param Thread $thread
     * @return mixed
     */
    public function destroy($channel, Thread $thread)
    {
        return $thread->unsubscribe();
    }
}
