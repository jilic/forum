<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterConfirmationController extends Controller
{

    /**
     * Confirm user email address.
     *
     * @param Request $request
     * @return \App\User
     * @internal param array $data
     */
    public function confirm(Request $request)
    {
        $user = User::where('confirmation_token', $request->get('token'))->first();

        if (! $user) {
            return redirect(route('thread.index'))
                ->with('flash', 'Unknown token.')
                ->with('flash-level', 'danger');
        }

        $user->confirm();

        return redirect(route('profiles.show', $user))
            ->with('flash', 'User email successfully confirmed.');
    }
}
