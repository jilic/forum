<?php

namespace App\Inspections;


class InvalidKeyword
{
    protected $keywords = [
        'yahoo customer support'
    ];

    /**
     * Detect invalid key words in spam.
     *
     * @param $body
     * @throws \Exception
     */
    public function detect($body)
    {

        foreach ($this->keywords as $keyword) {

            if (stripos($body, $keyword) !== false) {

                throw new \Exception('Your reply contains spam');
            }
        }
    }
}