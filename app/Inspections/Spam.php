<?php

namespace App\Inspections;


class Spam
{
    /**
     * All registered inspections.
     *
     * @var array
     */
    protected $inspections = [
        InvalidKeyword::class,
        KeyHeldDown::class
    ];

    /**
     * Detect all types of spam.
     *
     * @param $body
     * @return bool
     */
    public function detect($body)
    {
        foreach ($this->inspections as $inspection) {
            app($inspection)->detect($body);
        }

        return false;
    }
}