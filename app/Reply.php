<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use Favoritable, RecordsActivity;

    /**
     * Don't auto-apply mass assignment protection.
     *
     * @var array
     */
    protected $guarded = [];

    // moze biti problem ako imamo previse favorita (vise hiljada) po reply-ju
    // u komentarima lekcije 21 postoji interesantno resenje
    /**
     * Relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['owner', 'favorites'];

    /**
     * Custom attributes to append to json or array.
     *
     * @var array
     */
    protected $appends = ['favoritesCount', 'isFavorited', 'isBest'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($reply) {
           $reply->thread->increment('replies_count');
        });

        static::deleted(function ($reply) {
            if ($reply->isBest()) {
                $reply->thread->update(['best_reply_id' => null]);
            }
           $reply->thread->decrement('replies_count');
        });
    }


    /**
     * A reply has an owner.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * A reply belongs to thread.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    /**
     * Default path for thread.
     *
     * @return mixed
     */
    public function path()
    {
        return $this->thread->path();
    }

    /**
     * Is reply published less than minute ago.
     *
     * @return bool
     */
    public function wasJustPublished()
    {
        return Carbon::now()->diffInMinutes($this->created_at) < 1;
    }

    /**
     * All users mentioned in reply.
     * 
     * @return mixed
     */
    public function mentionedUsers()
    {
        preg_match_all('/\@([\w\-]+)/', $this->body, $matches);

        return $matches[1];

    }

    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = preg_replace('/\@([\w\-]+)/', '<a href="/profiles/$1">$0</a>', $body);
    }


    public function isBest()
    {
        return $this->thread->best_reply_id == $this->id;
    }


    public function getIsBestAttribute()
    {
        return $this->isBest();
    }
}
