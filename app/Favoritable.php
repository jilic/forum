<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

trait Favoritable
{


    /**
     * Define actions to be taken on model events.
     *
     * Boots in model witch use this trait.
     */
    protected static function bootFavoritable()
    {
        if (auth()->guest()) return;

        static::deleting(function($model) {
            $model->favorites->each->delete();
        });
    }

    /**
     * Model polymorphic relation to favorites.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorited');
    }

    /**
     * Favorite the current reply.
     *
     * @return Model
     */
    public function favorite()
    {
        $attributes = ['user_id' => auth()->id()];

        if (!$this->favorites()->where($attributes)->exists()) {

            return $this->favorites()->create($attributes);
        }
    }

    /**
     * Unfavorite the current reply.
     *
     * @return Model
     */
    public function unfavorite()
    {
        $attributes = ['user_id' => auth()->id()];

            return $this->favorites()->where($attributes)->get()->each->delete();
    }

    /**
     * Checks if user already favorited this reply.
     *
     * @return bool
     */
    public function isFavorited()
    {
        return !!$this->favorites->where('user_id', auth()->id())->count();
    }

    /**
     *  Access to isFavorited as attribute.
     *
     * @return bool
     */
    public function getIsFavoritedAttribute()
    {
        return $this->isFavorited();
    }

    /**
     * Returns number of favorites for the reply.
     *
     * @return mixed
     */
    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }
}