<?php

namespace App;


use Illuminate\Support\Facades\Redis;

trait RecordsVisits
{

    /**
     * Increments thread visits count.
     *
     * @return mixed
     */
    public function recordVisits()
    {
        Redis::incr($this->visitsCacheKey());

        return $this;
    }

    /**
     * Count of thread visits.
     *
     * @return mixed
     */
    public function visits()
    {
        return Redis::get($this->visitsCacheKey()) ?: 0;
    }

    /**
     * Redis key for thread visits count.
     *
     * @return string
     */
    public function visitsCacheKey(): string
    {
        return 'threads.' . $this->id . '.visits';
    }

    /**
     * Reset visits for a thread.
     */
    public function resetVisits()
    {
        Redis::del($this->visitsCacheKey());

        return $this;
    }
}