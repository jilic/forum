<?php

namespace App;

use App\Events\ThreadReceivedNewReply;
use App\Filter\ThreadsFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use RecordsActivity, RecordsVisits;

    /**
     * Don't auto-apply mass assignment protection.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['creator', 'channel'];

    /**
     * Always appends attribute to thread.
     *
     * @var array
     */
    protected $appends = ['isSubscribedTo'];

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();
        // Dodata je posebna kolona replies_count u tabelu thread
//        static::addGlobalScope('replyCount', function ($builder) {
//            $builder->withCount('replies');
//        });
        static::deleting(function ($thread) {
            $thread->replies->each->delete();
        });
    }

    /**
     * Slug column is used as route key.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get a string path for the thread.
     *
     * @return string
     */
    public function path()
    {
        return "/threads/{$this->channel->slug}/{$this->slug}";
    }

    /**
     * A thread belongs to a creator.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * A thread is assigned a channel.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * A thread may have many replies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * Add a reply to the thread.
     *
     * @param  array $reply
     * @return Reply|Model
     */
    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        event(new ThreadReceivedNewReply($reply));

        return $reply;
    }

    /**
     * Apply all relevant thread filters.
     *
     * @param  Builder $query
     * @param ThreadsFilter $filters
     * @return Builder
     */
    public function scopeFilter($query, ThreadsFilter $filters)
    {
        return $filters->apply($query);
    }

    /**
     * Return all subscribes related to the thread.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {

        return $this->hasMany(ThreadSubscription::class);

    }

    /**
     * Subscribe to user to the thread.
     *
     * @param null $userId
     * @return Model
     */
    public function subscribe($userId = null)
    {
        $this->subscriptions()->create(
            ['user_id' => $userId ? $userId : auth()->id()]
        );

        return $this;
    }

    /**
     * Unsubscribe user from the thread.
     *
     * @param null $userId
     */
    public function unsubscribe($userId = null)
    {
        $this->subscriptions()
            ->where('user_id', $userId ? $userId : auth()->id())
            ->delete();
    }

    /**
     * Returns if user is subscribed to the thread.
     *
     * @return bool
     */
    public function getIsSubscribedToAttribute()
    {
        return !!$this->subscriptions()
            ->where('user_id', auth()->id())
            ->exists();
    }

    /**
     * Mutator for slug column.
     *
     * @param $value
     */
    public function setSlugAttribute($value)
    {

        $slug = str_slug($value);

        for ($i = 1; self::where('slug', $slug)->exists(); $i++) {
            $slug = $slug . '-' . $i;
        }

        $this->attributes['slug'] = $slug;
    }

    /**
     * Check if user read all replies to thread.
     *
     * @return bool
     */
    public function hasUpdatesFor()
    {
        return $this->updated_at > cache(auth()->user()->visitedThreadCacheKey($this));
    }

    /**
     * Marks reply as best.
     *
     * @param Reply $reply
     */
    public function markBestReply(Reply $reply)
    {
        $this->best_reply_id = $reply->id;

        $this->save();
    }
}