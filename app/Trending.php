<?php

namespace App;


use Illuminate\Support\Facades\Redis;

class Trending
{
    /**
     * Return most trending threads.
     *
     * @return array
     */
    public function get()
    {
        return array_map('json_decode', Redis::zrevrange($this->cacheKey(), 0, 4));
    }

    /**
     * Increase counter for visited thread.
     *
     * @param $thread
     */
    public function push($thread)
    {
        Redis::zIncrBy($this->cacheKey(), 1, json_encode([
            'title' => $thread->title,
            'path' => $thread->path()
        ]));
    }

    /**
     * Cache key in Redis.
     *
     * @return string
     */
    public function cacheKey(): string
    {
        return app()->environment('testing') ? 'testing_trending_threads' : 'trending_threads';
    }

    /**
     * Empty Redis for given key.
     */
    public function reset()
    {
        Redis::del($this->cacheKey());
    }


}