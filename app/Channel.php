<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{


    /**
     * Uses slug as route key name.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }
}
