<?php

namespace App\Policies;

use App\Reply;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the reply.
     *
     * @param  \App\User $user
     * @param Reply $reply
     * @return mixed
     */
    public function update(User $user, Reply $reply)
    {
        return $user->id == $reply->user_id;
    }

    /**
     * Determine whether the user can create the reply.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        if(! $lastReply = $user->fresh()->lastReply) {
            return true;
        }
        return ! $lastReply->wasJustPublished();
    }
}
