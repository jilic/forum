<?php

namespace Tests\Unit;

use App\Reply;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_fetch_their_most_recent_reply()
    {
        $user = create(User::class);

        create(Reply::class, ['user_id' => $user->id]);
        $reply = create(Reply::class, [
            'user_id' => $user->id,
            'created_at'=> Carbon::now()->addMinutes(2)]);

        $this->assertEquals($reply->id, $user->lastReply->id);
    }

    /** @test */
    public function a_user_can_determine_avatar_path()
    {
        $user = create(User::class);

        self::assertEquals(asset('storage/avatars/avatar.png'), $user->avatar_path);

        $user->avatar_path = 'avatars/me.jpg';

        self::assertEquals(asset('storage/avatars/me.jpg'), $user->avatar_path);
    }
}