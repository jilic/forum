<?php

namespace Tests\Feature;

use App\Reply;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReplyTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function it_has_an_owner()
    {
        $reply = create(Reply::class);

        $this->assertInstanceOf(User::class, $reply->owner);
    }
    
     /** @test */
    public function it_knows_if_it_was_just_published()
    {
        $reply = create(Reply::class);

        $this->assertTrue($reply->wasJustPublished());
    }

    /** @test */
    public function it_can_return_all_mentioned_users_in_the_body()
    {
        $reply = new Reply([
            'body' => '@Pera and @Mika are mentioned in this reply.'
        ]);

        $this->assertEquals(['Pera','Mika'],$reply->mentionedUsers());
    }
    
    /** @test */
    public function it_wraps_mentioned_usernames_in_body_with_anchor_tags()
    {
        $reply = new Reply([
            'body' => 'Hello @Pe-ra.'
        ]);

        $this->assertEquals(
            'Hello <a href="/profiles/Pe-ra">@Pe-ra</a>.',
            $reply->body
        );
    }
    
    /** @test */
    public function it_knows_if_it_is_the_best_reply()
    {
        $reply = create(Reply::class);

        $this->assertFalse($reply->isBest());

        $reply->thread->update(['best_reply_id' => $reply->id]);

        $this->assertTrue($reply->fresh()->isBest());
    }
}