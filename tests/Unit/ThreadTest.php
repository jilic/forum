<?php

namespace Tests\Unit;

use App\Notifications\ThreadWasUpdated;
use App\User;
use App\Reply;
use App\Thread;
use App\Channel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;


    protected $thread;

    public function setUp()
    {

        parent::setUp();

        $this->thread = create(Thread::class);


    }


    /** @test */
    public function a_thread_can_make_a_path()
    {
        $this->assertEquals("/threads/{$this->thread->channel->slug}/{$this->thread->slug}", $this->thread->path());
    }

    /** @test */
    public function a_thread_has_a_creator()
    {

        $this->assertInstanceOf(User::class, $this->thread->creator);

    }


    /** @test */
    public function a_thread_has_replies()
    {
        create(Reply::class, ['thread_id' => $this->thread->id]);

        $this->assertInstanceOf(Collection::class, $this->thread->replies);
        $this->assertInstanceOf(Reply::class, $this->thread->replies->first());
    }


    /** @test */
    public function a_thread_can_add_reply()
    {
        $this->thread->addReply([
            'body' => 'foobar',
            'user_id' => 1
        ]);

        $this->assertCount(1, $this->thread->replies);
    }


    /** @test */
    public function a_thread_belongs_to_chanel()
    {
        $thread = create(Thread::class);

        $this->assertInstanceOf(Channel::class, $thread->channel);
    }
    
    /** @test */
    public function a_thread_can_be_subscribed_to()
    {
        // Given we have a thread and a user
        $userId = 1;

        // When the user subscribes to the thread
        $this->thread->subscribe($userId);

        // Then we should be able to fetch all threads that the user has subscribed to.
        $this->assertEquals(1, $this->thread->subscriptions()->where('user_id', $userId)->count());
    }

    /** @test */
    public function a_thread_can_be_unsubscribed_from ()
    {
        // Given we have thread and user subscribed to it
        $userId = 1;
        $this->thread->subscribe($userId);

        // When the thread is unsubscribed from
        $this->thread->unsubscribe($userId);

        // Then the thread should have no subscriptions
        $this->assertEquals(0, $this->thread->subscriptions()->where('user_id', $userId)->count());
    }
    
    /** @test */
    public function it_knows_if_authenticated_user_is_subscribed_to_id()
    {
        $this->signIn();
        $thread = create(Thread::class);

        $this->assertFalse($thread->isSubscribedTo);


        $thread->subscribe();

        $this->assertTrue($thread->isSubscribedTo);

    }
    
    /** @test */
    public function a_thread_notifies_all_registered_subscribes_when_reply_is_added()
    {
        Notification::fake();

        $this->signIn()->thread->subscribe()->addReply([
            'body' => 'foobar',
            'user_id' => 1
        ]);

        Notification::assertSentTo(auth()->user(), ThreadWasUpdated::class);
    }
    
    /** @test */
    public function a_thread_can_check_if_authenticated_user_has_read_all_replies()
    {
        $this->signIn();
        $user = auth()->user();

        $this->assertTrue($this->thread->hasUpdatesFor());

        $user->read($this->thread);

        $this->assertFalse($this->thread->hasUpdatesFor());
    }

    /** @test */
    public function a_thread_records_each_visit()
    {
        $thread = make(Thread::class, ['id' => 1]);

        $thread->resetVisits();
        $this->assertSame(0, $thread->visits());

        $thread->recordVisits();
        $this->assertEquals(1, $thread->visits());

        $thread->recordVisits();
        $this->assertEquals(2, $thread->visits());
    }
}
