<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BestReplyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_thread_creator_may_mark_any_reply_as_the_best_answer()
    {
        $this->signIn();
        $thread = create(Thread::class, ['user_id' => auth()->id()]);
        $replies = create(Reply::class, ['thread_id' => $thread->id], 2);

        $this->postJson(route('best-reply.store', $replies[1]));

        $this->assertTrue($replies[1]->fresh()->isBest());
        $this->assertFalse($replies[0]->fresh()->isBest());
    }

    /** @test */
    public function only_thread_creator_may_mark_reply_as_the_best_answer()
    {
        $this->withExceptionHandling();
        $this->signIn();
        $thread = create(Thread::class);
        $reply = create(Reply::class, ['thread_id' => $thread->id]);

        $this->postJson(route('best-reply.store', $reply))
            ->assertStatus(403);
        $this->assertFalse($reply->fresh()->isBest());
    }

    /** @test */
    public function if_a_best_reply_is_deleted_then_the_thread_is_properly_updated()
    {
        // Given user is signed in
        $this->signIn();

        // And thread has reply witch is marked as best
        $thread = create(Thread::class, ['user_id' => auth()->id()]);
        $reply = create(Reply::class, ['user_id' => auth()->id(), 'thread_id' => $thread->id]);
        $this->postJson(route('best-reply.store', $reply));

        $this->assertTrue($reply->fresh()->isBest());

        // When the best reply is deleted
        $this->delete("/replies/{$reply->id}")->assertStatus(302);

        // Then best_reply_id field in thread is set to null
        $this->assertSame(null, $thread->fresh()->best_reply_id);
    }
}
