<?php

namespace Tests\Feature;

use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Notifications\DatabaseNotification;
use Tests\TestCase;

class NotificationsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        // Given we have authenticated user in all tests
        $this->signIn();
    }

    /** @test */
    public function a_notification_is_prepared_when_a_subscribed_thread_receives_a_new_reply_that_is_not_by_current_user()
    {

        // And a subscribed thread
        $thread = create(Thread::class)->subscribe();

        // Check if notification table is empty
        $this->assertCount(0, auth()->user()->notifications);

        // Then, each time a reply is left there should be one notification per subscriber in database
        $thread->addReply([
            'user_id' => auth()->id(),
            'body' => "Some reply body text here"
        ]);

        // But not if we leave the reply there should no be notification to us
        $this->assertCount(0, auth()->user()->fresh()->notifications);

        // If somebody else leaves reply
        $thread->addReply([
            'user_id' => create(User::class)->id,
            'body' => "Some reply body text here"
        ]);

        // Then we should get notification
        $this->assertCount(1, auth()->user()->fresh()->notifications);
    }

    /** @test */
    public function a_user_can_fetch_their_unread_notification()
    {
        create(DatabaseNotification::class);

        $response = $this->getJson("/profiles/". auth()->user()->name . "/notifications")->json();

        $this->assertCount(1, $response);
    }

    /** @test */
    public function a_user_can_mark_a_notification_as_read()
    {
        create(DatabaseNotification::class);

        // Prosledjuje sadrzaj prvog argumenta u callback fju
        tap(auth()->user(), function ($user) {
            // User should have one unread notification
            $this->assertCount(1, $user->unreadNotifications);


            $notificationId = $user->unreadNotifications->first()->id;

            $this->delete("/profiles/". $user->name . "/notifications/" . $notificationId);

            $this->assertCount(0, $user->fresh()->unreadNotifications);
        });

    }
}
