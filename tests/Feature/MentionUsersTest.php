<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MentionUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function metioned_users_in_reply_are_notified()
    {
        // Given we have a signed in user Marko
        $marko = create(User::class, ['name' => 'Marko']);
        $this->signIn($marko);

        // And user Pera
        $pera = create(User::class, ['name' => 'Pera']);

        // When we have a thread
        $thread = create(Thread::class);
        // And Marko replies and mentions Pera
        $reply = make(Reply::class, [
            'thread_id' => $thread->id,
            'user_id' => $marko->id,
            'body' => 'In this reply @Pera is mentioned.'
        ]);

        // Then Pera should be notified
        $this->json('post', $thread->path() . '/replies', $reply->toArray());

        $this->assertCount(1, $pera->notifications);
    }
}
