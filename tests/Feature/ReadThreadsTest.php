<?php

namespace Tests\Feature;

use App\Channel;
use App\Reply;
use App\Thread;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadThreadsTest extends TestCase
{
    use RefreshDatabase;


    protected $thread;

    public function setUp()
    {
        parent::setUp();

        $this->thread = create(Thread::class);

    }


    /** @test */
    public function a_user_can_view_all_threads()
    {

        $this->get('/threads')
            ->assertSee($this->thread->title);

    }


    /** @test */
    public function a_user_can_view_single_thread()
    {

        $this->get($this->thread->path())
            ->assertSee($this->thread->title);

    }


    /** @test */
    public function a_user_can_filter_threads_according_to_a_channel()
    {
        $channel = create(Channel::class);
        $threadInChannel = create(Thread::class, ['channel_id' => $channel->id]);
        $threadNotInChannel = create(Thread::class);

        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_any_username()
    {
        $this->signIn(create(User::class, ['name' => 'JohnDoe']));

        $threadByJohnDoe = create(Thread::class, ['user_id' => auth()->id()]);
        $threadNotByJohnDoe = create(Thread::class);

        $this->get('/threads?by=JohnDoe')
            ->assertSee($threadByJohnDoe->title)
            ->assertDontSee($threadNotByJohnDoe->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_popularity()
    {
        // Given we have three threads
        // With 3 replies, 2 replies and 0 replies respectively
        $threadWithTwoReplies = create(Thread::class);
        create(Reply::class, ['thread_id' => $threadWithTwoReplies->id], 2);

        $threadWithThreeReplies = create(Thread::class);
        create(Reply::class, ['thread_id' => $threadWithThreeReplies->id], 3);

        $threadWithNoReplies = $this->thread;

        // When we filter all threads by popularity
        $response = $this->get('/threads?popular=1'); //->json();

        // Then they should return from most replies to least
        $threadsFromResponse = $response->baseResponse->original->getData()['threads'];

        $this->assertEquals([3, 2, 0], $threadsFromResponse->pluck('replies_count')->toArray(),
            'Popular thread order is not as expected');

    }

    /** @test */
    public function a_user_can_filter_unanswered_threads()
    {
        // Given we have three threads
        // With 3 replies, 2 replies and 0 replies respectively
        $threadWithThreeReplies = create(Thread::class);
        create(Reply::class, ['thread_id' => $threadWithThreeReplies->id], 3);

        $threadWithTwoReplies = create(Thread::class);
        create(Reply::class, ['thread_id' => $threadWithTwoReplies->id], 2);

        $threadWithNoReplies = $this->thread;

        // When we filter all threads by popularity
        $response = $this->get('/threads?unanswered=1'); //->json();

        // Then they should return only unanswered threads
        $threadsFromResponse = $response->baseResponse->original->getData()['threads']->pluck('replies_count');

        $this->assertCount(1, $threadsFromResponse->toArray(),
            'There are more threads than expected');

        $this->assertEquals(0, $threadsFromResponse->toArray()[0],
            'Count of replies is not equal to 0');

    }

    /** @test */
    public function a_user_can_request_all_replies_for_a_given_thread()
    {
        $thread = create(Thread::class);
        $reply = create(Reply::class, ['thread_id' => $thread->id], 2);

        $response = $this->getJson($thread->path().'/replies')->json();

        $this->assertCount(2, $response['data']);
        $this->assertEquals(2, $response['total']);
    }

}