<?php

namespace Tests\Feature;

use App\Activity;
use App\Channel;
use App\Reply;
use App\Thread;
use App\User;
use phpDocumentor\Reflection\Location;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateThreadsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guest_may_not_create_threads_or_see_create_page()
    {
        // Use laravel built in exception handling and exception will not be thrown
        $this->withExceptionHandling();

        $this->post('/threads')
            ->assertRedirect('/login');

        $this->get('/threads/create')
            ->assertRedirect('/login');

    }

    /** @test */
    public function new_users_must_first_confirm_email_address_before_creating_new_thread()
    {
        $user = factory(User::class)->states('unconfirmed')->create();

        $this->withExceptionHandling()->signIn($user);

        $thread = make(Thread::class);

        return $this->post('/threads', $thread->toArray())
            ->assertRedirect('/threads')
            ->assertSessionhas('flash', 'You must first confirm your email address');
    }

    /** @test */
    public function an_authenticated_confirmed_user_can_crate_new_forum_thread()
    {
        // Given we have an authenticated confirmed user
        $this->signIn();

        // When we hit the endpoint to create new thread
        $thread = make(Thread::class);
        $response = $this->post('/threads', $thread->toArray());

        // Then when we visit the thread page, should see new thread
        $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }


    /** @test */
    public function it_requires_a_title()
    {
        $this->publishAThread(['title' => null])
            ->assertSessionHasErrors('title');
    }


    /** @test */
    public function it_requires_a_body()
    {
        $this->publishAThread(['body' => null])
            ->assertSessionHasErrors('body');
    }


    /** @test */
    public function it_requires_a_valid_channel()
    {
        create(Channel::class, [], 2);

        $this->publishAThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

        $this->publishAThread(['channel_id' => 999999])
            ->assertSessionHasErrors('channel_id');
    }

    /** @test */
    public function it_requires_unique_slug()
    {
        $this->signIn();

        $thread = create(Thread::class,
            ['title' => 'new title',
             'slug' => 'new-title',
             'user_id' => auth()->id()
            ]);

        $this->post(route('thread.store', $thread->toArray()));

        $this->assertTrue(Thread::where('slug', 'new-title-1')->exists());
    }

    /** @test */
    public function unauthorized_users_cannot_delete_threads()
    {
        $this->withExceptionHandling();

        $thread = create(Thread::class);

        // Guest cannot delete thread
        $this->delete($thread->path())->assertRedirect('/login');

        $this->signIn();

        // Thread witch is not created by authenticated user must not be deleted
        $this->delete($thread->path())->assertStatus(403);
    }

    /** @test */
    public function authorized_users_can_delete_threads_and_associated_replies_and_activities()
    {
        $this->signIn();

        $thread = create(Thread::class, ['user_id' => auth()->id()]);
        $reply = create(Reply::class, ['thread_id' => $thread->id]);

        $response = $this->json('DELETE', $thread->path());

        $response->assertStatus(204);

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        $this->assertEquals(0, Activity::count());
    }

    /** @test */
    public function thread_body_cannot_contain_spam_on_creation()
    {
        $this->signIn();

        $thread = make(Thread::class, ['body' => 'aaaaaaaaa']);

        $this->expectException(\Exception::class);

        $this->post('/threads', $thread->toArray());
    }

    protected function publishAThread($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $thread = make(Thread::class, $overrides);

        return $this->post('/threads', $thread->toArray());
    }
}
