<?php

namespace Tests\Feature;

use App\Favorite;
use App\Reply;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FavoritesTest extends TestCase
{
    use RefreshDatabase;


    /** @test */
    public function a_guest_cannot_favorite_anything()
    {
        $this->withExceptionHandling()
             ->post('/replies/1/favorites')
             ->assertRedirect('/login');
    }


    /** @test */
    public function an_authenticated_user_can_favorite_a_reply()
    {
        $this->signIn();

        $reply = create(Reply::class);

        $this->post('/replies/' . $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);
    }

    /** @test */
    public function an_authenticated_user_can_unfavorite_a_reply_and_delete_associated_activities()
    {
        $this->signIn();
        $reply = create(Reply::class);
        $reply->favorite();
        $favorite = $reply->favorites->first();

        $this->delete('/replies/' . $reply->id . '/favorites');
        // ponovo ucitavamo rezultate da ne bili ucitani stari iz kesa
        $this->assertCount(0, $reply->fresh()->favorites);
        $this->assertDatabaseMissing('activities', [
            'subject_id' => $favorite->id,
            'subject_type' => Favorite::class
            ]);

    }
    
    /** @test */
    public function an_authenticated_user_may_only_favorite_a_reply_once()
    {
        $this->signIn();

        $reply = create(Reply::class);

        $this->post('/replies/' . $reply->id . '/favorites');
        $this->post('/replies/' . $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);

    }
}
