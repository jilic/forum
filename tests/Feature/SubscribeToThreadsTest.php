<?php

namespace Tests\Feature;

use App\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscribeToThreadsTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_user_can_subscribe_to_threads()
    {
        // Given we have authenticated user
        $this->signIn();

        // And a thread
        $thread = create(Thread::class);

        // When user subscribes to the thread
        $this->post($thread->path().'/subscriptions');

        // Then there should be one subscription
        $this->assertCount(1, $thread->fresh()->subscriptions);
    }

    /** @test */
    public function a_user_can_unsubscribe_from_threads()
    {
        // Given we have authenticated user
        $this->signIn();

        // And a subscribed thread
        $thread = create(Thread::class);
        $thread->subscribe();

        // When user unsubscribes to the thread
        $this->delete($thread->path().'/subscriptions');

        // Then, each time a reply is left
        $this->assertEquals(0, $thread->subscriptions()->where('user_id', auth()->id())->count());
    }
}
