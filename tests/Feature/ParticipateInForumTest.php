<?php

namespace Tests\Feature;

use App\Activity;
use App\Notifications\ThreadWasUpdated;
use App\Reply;
use App\Thread;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParticipateInForumTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function unauthenticated_user_may_not_add_replies()
    {
        $this->withExceptionHandling();

        $this->post('threads/some-slug/1/replies', [])
            ->assertRedirect('/login');
    }

    /** @test */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {
        // Given we have an authenticated user
        $this->signIn();

        // And existing thread
        $thread = create(Thread::class);

        // When a user adds a reply to a thread
        $reply = make(Reply::class, ['thread_id' => $thread->id]);
        $this->post($thread->path() . '/replies', $reply->toArray());

        // Then their reply should be stored in database and threads replies_count should be increased by 1
        $this->assertDatabaseHas('replies', ['body' => $reply->body]);
        $this->assertEquals(1, $thread->fresh()->replies_count);
    }

    /** @test */
    public function a_reply_requires_a_body()
    {
        $this->withExceptionHandling()->signIn();

        $reply = make(Reply::class, ['body' => null]);
        $this->json('post',$reply->thread->path() . '/replies', $reply->toArray())
                 ->assertStatus(422);
    }

    /** @test */
    public function unauthorized_or_unauthenticated_users_cannot_delete_replies()
    {
        $this->withExceptionHandling();

        $reply = create(Reply::class);

        $this->delete("/replies/{$reply->id}")
            ->assertRedirect('/login');

        $this->signIn();

        $this->delete("/replies/{$reply->id}")
            ->assertStatus(403);
    }


    /** @test */
    public function authorized_users_can_delete_replies_and_associated_activities_and_favorites()
    {
        // Given user is signed in
        $this->signIn();

        // And we have a reply that belongs to thread and that reply is favorited
        $thread = create(Thread::class);
        $reply = create(Reply::class, ['user_id' => auth()->id(), 'thread_id' => $thread->id]);
        $reply->favorite();
        $favorite = $reply->favorites->first();

        // Then after reply delete database missing the reply and replies_count in threads table is decreased by 1
        $this->delete("/replies/{$reply->id}")->assertStatus(302);
        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        $this->assertEquals(0, $thread->fresh()->replies_count);
        // And associated activities and favorites for reply
        $this->assertDatabaseMissing('activities', [
            'subject_id' => $reply->id,
            'subject_type' => get_class($reply)
        ]);
        $this->assertDatabaseMissing('favorites',  [
            'favorited_id' => $reply->id,
            'favorited_type' => get_class($reply)
        ]);
        // And associated activities for favorites
        $this->assertDatabaseMissing('activities', [
            'subject_id' => $favorite->id,
            'subject_type' => get_class($favorite)
        ]);
    }

    /** @test */
    public function unauthorized_or_unauthenticated_users_cannot_update_replies()
    {
        $this->withExceptionHandling();

        $reply = create(Reply::class);

        $this->patch("/replies/{$reply->id}")
            ->assertRedirect('/login');

        $this->signIn();

        $this->patch('/replies/' . $reply->id, ['body' => '123'])
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_users_can_update_the_reply()
    {
        $this->signIn();

        $reply = create(Reply::class, ['user_id' => auth()->id()]);

        $updatedReply = 'new data';
        $this->patch("/replies/{$reply->id}", ['body' => $updatedReply]);

        $this->assertDatabaseHas('replies', [
            'id' => $reply->id,
            'body' => $updatedReply
        ]);
    }
    
    /** @test */
    public function replies_that_contain_spam_may_not_be_created()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create(Thread::class);

        $reply = make(Reply::class, [
            'thread_id' => $thread->id,
            'body' => 'Yahoo Customer Support'
        ]);

        $this->json('post',$thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

    /** @test */
    public function replies_cannot_be_updated_with_invalid_data()
    {
        $this->withExceptionHandling();
        $this->signIn();

        $reply = create(Reply::class, [
            'body' => 'Body text without spam.',
            'user_id' => auth()->id()
        ]);

        $this->json('patch', '/replies/' . $reply->id, ['body' => 'wwwwwwww'])
            ->assertStatus(422);
    }
    
    /** @test */
    public function users_may_reply_maximum_once_per_minute()
    {
        $this->withExceptionHandling();
        $this->signIn();

        $thread = create(Thread::class);

        $reply = make(Reply::class, [
            'user_id' => auth()->id(),
            'thread_id' => $thread->id
        ]);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(200);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(429);
    }
}