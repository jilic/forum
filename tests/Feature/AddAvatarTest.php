<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AddAvatarTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_members_can_add_avatars()
    {
        $this->withExceptionHandling();

        $response = $this->json('POST','api/users/1/avatar', ['avata' => 'shouldn\'t get to validation']);

        $response->assertStatus(401);
    }
    
    /** @test */
    public function a_valid_avatar_must_be_provided()
    {
        $this->withExceptionHandling()->signIn();

        $this->json('POST','api/users/{user}/avatar', [
            'avatar' => 'not valid avatar data'
        ])->assertStatus(422);
    }
    
    /** @test */
    public function an_user_may_add_an_avatar_to_their_profile()
    {
        $this->signIn();

        Storage::fake('public');

        $this->json('POST','api/users/{user}/avatar', [
            'avatar' => $file = UploadedFile::fake()->image('avatar.jpg')
        ]);

        $this->assertDatabaseHas('users',
            [
                'id' => auth()->id(),
                'avatar_path' => 'avatars/' . $file->hashName()
            ]);

        Storage::disk('public')->assertExists('avatars/' . $file->hashName());
    }


}
