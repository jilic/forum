<?php

namespace Tests\Feature;

use App\Mail\PleaseConfirmYourEmail;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function confirmation_email_is_sent_upon_registration()
    {
        Mail::fake();

        $this->post(route('register', [
            'name'                  => 'korisnik',
            'email'                 => 'korisnik@forum.com',
            'password'              => 'admin11',
            'password_confirmation' => 'admin11'
        ]));

        Mail::assertSent(PleaseConfirmYourEmail::class);
    }

    /** @test */
    public function users_can_confirm_their_email_address()
    {
        // Test se izvrsava znatno sporije ako stvarno posaljemo mail
        Mail::fake();

        $this->post(route('register', [
            'name'                  => 'korisnik',
            'email'                 => 'korisnik@forum.com',
            'password'              => 'admin11',
            'password_confirmation' => 'admin11'
        ]));
        $user = User::where('name', 'korisnik')->first();

        $this->assertFalse($user->confirmed);

        $this->assertNotNull($user->confirmation_token);

        $this->get(route('register.confirm', ['token' => $user->confirmation_token]))
            ->assertRedirect(route('profiles.show', $user));

        tap($user->fresh(), function($user) {
            $this->assertTrue($user->confirmed);
            $this->assertNull($user->confirmation_token);
        });
    }
    
    /** @test */
    public function confirming_an_invalid_token()
    {
        $this->get(route('register.confirm', ['token' => 'invalid']))
            ->assertRedirect(route('thread.index'))
            ->assertSessionhas('flash', 'Unknown token.');
    }
}
