<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    /**
     * Disables Laravel exception handling in all tests.
     */
    public function setUp()
    {

        parent::setUp();

        $this->withoutExceptionHandling();

    }

    /**
     * Set new or given user as logged in for application.
     *
     * @param null $user
     * @return $this
     */
    public function signIn($user = null)
    {
        $user = $user ?: create(User::class);

        $this->be($user);

        return $this;
    }
}
