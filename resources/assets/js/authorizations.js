let user = window.App.user;

let authorization = {
    /*
    updateReply: function (reply) {
        return reply.user_id === user.id;
    },
    updateThread: function (thread) {
        return thread.user_id === user.id;
    },*/
    owns: function (model, props = 'user_id') {
        return model[props] === user.id;
    }
};

module.exports = authorization;